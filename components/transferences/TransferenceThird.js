//var moment = require('moment');
//moment.locale('es');

import React, {Component} from 'react';
import {
  Text,
  View,
  Button,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';

export default class TransferenceThird extends Component {
  constructor(props) {
    super(props);
    this.state = {
      importe: 0,
    };
  }
  render() {
    return (
      <View>
        {/* <ImageBackground
          source={require('../../img/okimg.jpg')}
          style={{resizeMode: 'cover', justifyContent: 'center'}}> */}
          <View style={{alignItems: 'center',marginTop:160,}}>
            <Image
              source={require('../../img/okimg2.png')}
              style={{
                aspectRatio: 1,
                resizeMode: 'contain',
                height:'70%',
                

              }}
            />
            <TouchableOpacity
              style={{
                height: 40,
                width:'30%',
                borderRadius: 5,
                backgroundColor: '#2753e3',
                justifyContent: 'center',
                alignItems:'center',
                marginTop:50,
              }}
              onPress={() => this.props.navigation.navigate('App')}
              >
              <Text style={{fontSize: 25, color: 'white'}}>Aceptar</Text>
            </TouchableOpacity>
          </View>
        {/* </ImageBackground> */}
      </View>
    );
  }
}
