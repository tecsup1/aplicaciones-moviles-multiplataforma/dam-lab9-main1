import React, {Component} from 'react';
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Alert,
  Button,
  ImageComponent,
  Image,
  PickerIOSItem,
  Dimensions,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default class Maps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      places: [
        {
          name: 'Tecsup Norte',
          coordinates: {
            latitude: -8.1494049,
            longitude: -79.0437666,
          },
        },
        {
          name: 'Tecsup Centro',
          coordinates: {
            latitude: -12.0447884,
            longitude: -76.9545117,
          },
        },
        {
          name: 'Tecsup Sur',
          coordinates: {
            latitude: -16.4288508,
            longitude: -71.5038502,
          },
        },
      ],

    };
  }


  render() {
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          initialRegion={{
            // The latitude and longitude specifies the center location of the
            //  map and latitudeDelta and longitudeDelta specify the view
            //  area for map
            latitude: -16.409046,
            longitude: -71.537453,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
  
          }}>
          {this.state.places.map((item, index) => {
            return (
              <Marker
                key={index}
                coordinate={{
                  latitude: item.coordinates.latitude,
                  longitude: item.coordinates.longitude,
                }}>
                <Callout>
                  <View
                    style={{
                      backgroundColor: '#FFFFFF',
                      borderRadius: 5,
                    }}>
                    <Text>{item.name}</Text>
                  </View>
                </Callout>
              </Marker>
            );
          })}
        </MapView>
        <TouchableOpacity
          onPress={() => {
            this.setState({latitudeDelta: this.state.latitudeDelta - 0.5});
            this.setState({longitudeDelta: this.state.longitudeDelta - 0.5});
            console.log("latitudeDelta: "+this.state.latitudeDelta);
          }}>
          <Text>Zoom in</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.setState({latitudeDelta: this.state.latitudeDelta + 0.5});
            this.setState({longitudeDelta: this.state.longitudeDelta + 0.5});
            console.log("longitudeDelta: "+this.state.longitudeDelta);

          }}>
          <Text>Zoom out</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  map: {
    ...StyleSheet.absoluteFillObject,

  },
});
