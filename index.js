/**
 * @format
 */

import {AppRegistry} from 'react-native';
import TransferenceStackScreen from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => TransferenceStackScreen);
